﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6Vjes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int hit = 0;
        int pokusaji = 8;
        string pojam;
        static Random rnd = new Random();

        List<Label> labels = new List<Label>();
        List<string> pojmovi = new List<string>();
        string path = "C:\\Users\\User\\Desktop\\LV6Analiza\\LV6Vjes\\rijeci.txt";
        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    pojmovi.Add(line);
                }
            }

            int r = rnd.Next(pojmovi.Count);
            pojam = (string)pojmovi[r];

            labels = new List<Label>();
            int startX = 80;
            foreach (char c in pojam)
            {
                Label lbl = new Label();
                lbl.Text = "_";
                lbl.Font = new Font(lbl.Font.Name, 20, lbl.Font.Style);
                lbl.Location = new Point(startX, 90);
                lbl.Tag = c.ToString();
                lbl.AutoSize = true;
                this.Controls.Add(lbl);
                labels.Add(lbl);
                startX = lbl.Right;
            }
        }
        private void button_pokusaj_Click(object sender, EventArgs e)
        {
            if (textBox_pokusaj.Text.Length == 0 || textBox_pokusaj.Text.Length > 1)
            {
                textBox_pokusaj.Text = "";
                MessageBox.Show("Pogrešan unos!", "Greška!");
            }
            else
            {
                if (pojam.Contains(textBox_pokusaj.Text))
                {
                    for (int i = 0; i < pojam.Length; i++)
                    {
                        if (pojam.IndexOf(textBox_pokusaj.Text) == pojam.IndexOf(pojam[i]))
                        {
                            if (labels[i].Text == "_")
                            {
                                labels[i].Text = textBox_pokusaj.Text;
                                hit++;
                            }
                            else
                            {
                                MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                break;
                            }
                        }
                    }
                    textBox_pokusaj.Text = "";
                    if (hit == pojam.Length)
                    {
                        MessageBox.Show("Pobjedio si!", "Pobjeda!");
                        Application.Exit();
                    }
                }
                else
                {
                    if (pokusaji == 1)
                    {
                        textBox_pokusaj.Text = "";
                        pokusaji--;
                        label_pokusaji.Text = "Broj preostalih pokusaja: " + pokusaji;
                        MessageBox.Show("Izgubio si!", "Poraz!");
                        Application.Exit();
                    }
                    else
                    {
                        textBox_pokusaj.Text = "";
                        pokusaji--;
                        label_pokusaji.Text = "Broj preostalih pokusaja: " + pokusaji;
                    }
                }
            }
        }

        private void button_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label_pokusaji_Click(object sender, EventArgs e)
        {

        }
    }
}
