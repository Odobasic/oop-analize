﻿namespace LV6Vjes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_pokusaj = new System.Windows.Forms.TextBox();
            this.button_pokusaj = new System.Windows.Forms.Button();
            this.label_pokusaji = new System.Windows.Forms.Label();
            this.button_izlaz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox_pokusaj
            // 
            this.textBox_pokusaj.Location = new System.Drawing.Point(115, 251);
            this.textBox_pokusaj.Multiline = true;
            this.textBox_pokusaj.Name = "textBox_pokusaj";
            this.textBox_pokusaj.Size = new System.Drawing.Size(23, 20);
            this.textBox_pokusaj.TabIndex = 0;
            // 
            // button_pokusaj
            // 
            this.button_pokusaj.Location = new System.Drawing.Point(12, 393);
            this.button_pokusaj.Name = "button_pokusaj";
            this.button_pokusaj.Size = new System.Drawing.Size(180, 71);
            this.button_pokusaj.TabIndex = 1;
            this.button_pokusaj.Text = "Pokusaj";
            this.button_pokusaj.UseVisualStyleBackColor = true;
            this.button_pokusaj.Click += new System.EventHandler(this.button_pokusaj_Click);
            // 
            // label_pokusaji
            // 
            this.label_pokusaji.AutoSize = true;
            this.label_pokusaji.Location = new System.Drawing.Point(28, 310);
            this.label_pokusaji.Name = "label_pokusaji";
            this.label_pokusaji.Size = new System.Drawing.Size(176, 17);
            this.label_pokusaji.TabIndex = 2;
            this.label_pokusaji.Text = "Broj preostalih pokusaja: 8";
            this.label_pokusaji.Click += new System.EventHandler(this.label_pokusaji_Click);
            // 
            // button_izlaz
            // 
            this.button_izlaz.Location = new System.Drawing.Point(401, 393);
            this.button_izlaz.Name = "button_izlaz";
            this.button_izlaz.Size = new System.Drawing.Size(114, 71);
            this.button_izlaz.TabIndex = 3;
            this.button_izlaz.Text = "Izlaz";
            this.button_izlaz.UseVisualStyleBackColor = true;
            this.button_izlaz.Click += new System.EventHandler(this.button_izlaz_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Vase slovo:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 476);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_izlaz);
            this.Controls.Add(this.label_pokusaji);
            this.Controls.Add(this.button_pokusaj);
            this.Controls.Add(this.textBox_pokusaj);
            this.Name = "Form1";
            this.Text = "Vjesalo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_pokusaj;
        private System.Windows.Forms.Button button_pokusaj;
        private System.Windows.Forms.Label label_pokusaji;
        private System.Windows.Forms.Button button_izlaz;
        private System.Windows.Forms.Label label1;
    }
}

