﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6Calc
{
    public partial class Calculator : Form
    {
        double FirstNumber;
        string Operation;


        public Calculator()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "1";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "1";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "2";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "2";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "3";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "3";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "4";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "4";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "5";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "5";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "6";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "6";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "7";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "7";
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "8";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "8";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "9";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "9";
            }
        }

        private void button0_Click(object sender, EventArgs e)
        {
            textBox_display.Text = textBox_display.Text + "0";
        }

        private void button_dot_Click(object sender, EventArgs e)
        {
            textBox_display.Text = textBox_display.Text + ".";
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_ce_Click(object sender, EventArgs e)
        {
            textBox_display.Text = "0";
            
        }

        private void button_plus_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = " ";
            Operation = "+";
        }

        private void button_minus_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = " ";
            Operation = "-";
        }

        private void button_puta_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = " ";
            Operation = "*";
        }

        private void button_div_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = " ";
            Operation = "/";
        }

        private void button_equal_Click(object sender, EventArgs e)
        {
            double SecondNumber;
            double Result;

            SecondNumber = Convert.ToDouble(textBox_display.Text);

            if (Operation == "+")
            {
                Result = (FirstNumber + SecondNumber);
                textBox_display.Text = Convert.ToString(Result);
                FirstNumber = 0;
            }
            if (Operation == "-")
            {
                Result = (FirstNumber - SecondNumber);
                textBox_display.Text = Convert.ToString(Result);
                FirstNumber = 0;
            }
            if (Operation == "*")
            {
                Result = (FirstNumber * SecondNumber);
                textBox_display.Text = Convert.ToString(Result);
                FirstNumber = 0;
            }
            if (Operation == "/")
            {
                if (SecondNumber == 0)
                {
                    textBox_display.Text = "Cannot divide by zero";

                }
                else
                {
                    Result = (FirstNumber / SecondNumber);
                    textBox_display.Text = Convert.ToString(Result);
                    FirstNumber = 0;
                }
            }
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = Math.Sqrt(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = Math.Log(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = Math.Sin(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = Math.Cos(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_nadr_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = Math.Pow( FirstNumber,  2);            
            textBox_display.Text = Result.ToString();
        }

        private void button_natr_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = Math.Pow(FirstNumber, 3);      
            textBox_display.Text = Result.ToString();
        }
    }
}
