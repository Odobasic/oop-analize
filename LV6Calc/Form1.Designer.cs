﻿namespace LV6Calc
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_display = new System.Windows.Forms.TextBox();
            this.button_ce = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_div = new System.Windows.Forms.Button();
            this.button_puta = new System.Windows.Forms.Button();
            this.button_minus = new System.Windows.Forms.Button();
            this.button_plus = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button_dot = new System.Windows.Forms.Button();
            this.button_equal = new System.Windows.Forms.Button();
            this.button_log = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_natr = new System.Windows.Forms.Button();
            this.button_nadr = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_display
            // 
            this.textBox_display.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.textBox_display.Location = new System.Drawing.Point(12, 42);
            this.textBox_display.Multiline = true;
            this.textBox_display.Name = "textBox_display";
            this.textBox_display.Size = new System.Drawing.Size(639, 62);
            this.textBox_display.TabIndex = 0;
            this.textBox_display.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_ce
            // 
            this.button_ce.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ce.Location = new System.Drawing.Point(12, 131);
            this.button_ce.Name = "button_ce";
            this.button_ce.Size = new System.Drawing.Size(174, 68);
            this.button_ce.TabIndex = 1;
            this.button_ce.Text = "CLEAR";
            this.button_ce.UseVisualStyleBackColor = true;
            this.button_ce.Click += new System.EventHandler(this.button_ce_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(102, 279);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(84, 68);
            this.button5.TabIndex = 7;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(192, 279);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(84, 68);
            this.button6.TabIndex = 6;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(12, 205);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(84, 68);
            this.button7.TabIndex = 5;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(192, 205);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(84, 68);
            this.button9.TabIndex = 10;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(102, 205);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(84, 68);
            this.button8.TabIndex = 9;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 279);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(84, 68);
            this.button4.TabIndex = 8;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(192, 353);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 68);
            this.button3.TabIndex = 13;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(102, 353);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 68);
            this.button2.TabIndex = 12;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 353);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 68);
            this.button1.TabIndex = 11;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_div
            // 
            this.button_div.Location = new System.Drawing.Point(282, 427);
            this.button_div.Name = "button_div";
            this.button_div.Size = new System.Drawing.Size(84, 68);
            this.button_div.TabIndex = 17;
            this.button_div.Text = "/";
            this.button_div.UseVisualStyleBackColor = true;
            this.button_div.Click += new System.EventHandler(this.button_div_Click);
            // 
            // button_puta
            // 
            this.button_puta.Location = new System.Drawing.Point(282, 353);
            this.button_puta.Name = "button_puta";
            this.button_puta.Size = new System.Drawing.Size(84, 68);
            this.button_puta.TabIndex = 16;
            this.button_puta.Text = "*";
            this.button_puta.UseVisualStyleBackColor = true;
            this.button_puta.Click += new System.EventHandler(this.button_puta_Click);
            // 
            // button_minus
            // 
            this.button_minus.Location = new System.Drawing.Point(282, 279);
            this.button_minus.Name = "button_minus";
            this.button_minus.Size = new System.Drawing.Size(84, 68);
            this.button_minus.TabIndex = 15;
            this.button_minus.Text = "-";
            this.button_minus.UseVisualStyleBackColor = true;
            this.button_minus.Click += new System.EventHandler(this.button_minus_Click);
            // 
            // button_plus
            // 
            this.button_plus.Location = new System.Drawing.Point(282, 205);
            this.button_plus.Name = "button_plus";
            this.button_plus.Size = new System.Drawing.Size(84, 68);
            this.button_plus.TabIndex = 14;
            this.button_plus.Text = "+";
            this.button_plus.UseVisualStyleBackColor = true;
            this.button_plus.Click += new System.EventHandler(this.button_plus_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(12, 427);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(84, 68);
            this.button0.TabIndex = 18;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // button_dot
            // 
            this.button_dot.Location = new System.Drawing.Point(102, 427);
            this.button_dot.Name = "button_dot";
            this.button_dot.Size = new System.Drawing.Size(84, 68);
            this.button_dot.TabIndex = 19;
            this.button_dot.Text = ".";
            this.button_dot.UseVisualStyleBackColor = true;
            this.button_dot.Click += new System.EventHandler(this.button_dot_Click);
            // 
            // button_equal
            // 
            this.button_equal.Location = new System.Drawing.Point(192, 427);
            this.button_equal.Name = "button_equal";
            this.button_equal.Size = new System.Drawing.Size(84, 68);
            this.button_equal.TabIndex = 20;
            this.button_equal.Text = "=";
            this.button_equal.UseVisualStyleBackColor = true;
            this.button_equal.Click += new System.EventHandler(this.button_equal_Click);
            // 
            // button_log
            // 
            this.button_log.Location = new System.Drawing.Point(575, 131);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(84, 68);
            this.button_log.TabIndex = 21;
            this.button_log.Text = "Log";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Location = new System.Drawing.Point(485, 131);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(84, 68);
            this.button_sqrt.TabIndex = 22;
            this.button_sqrt.Text = "√ ";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // button_cos
            // 
            this.button_cos.Location = new System.Drawing.Point(575, 205);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(84, 68);
            this.button_cos.TabIndex = 23;
            this.button_cos.Text = "Cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_sin
            // 
            this.button_sin.Location = new System.Drawing.Point(485, 205);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(84, 68);
            this.button_sin.TabIndex = 24;
            this.button_sin.Text = "Sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_natr
            // 
            this.button_natr.Location = new System.Drawing.Point(575, 279);
            this.button_natr.Name = "button_natr";
            this.button_natr.Size = new System.Drawing.Size(84, 68);
            this.button_natr.TabIndex = 25;
            this.button_natr.Text = "X^3";
            this.button_natr.UseVisualStyleBackColor = true;
            this.button_natr.Click += new System.EventHandler(this.button_natr_Click);
            // 
            // button_nadr
            // 
            this.button_nadr.Location = new System.Drawing.Point(485, 279);
            this.button_nadr.Name = "button_nadr";
            this.button_nadr.Size = new System.Drawing.Size(84, 68);
            this.button_nadr.TabIndex = 26;
            this.button_nadr.Text = "X^2";
            this.button_nadr.UseVisualStyleBackColor = true;
            this.button_nadr.Click += new System.EventHandler(this.button_nadr_Click);
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(485, 422);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(174, 73);
            this.button_exit.TabIndex = 27;
            this.button_exit.Text = "Exit";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 510);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.button_nadr);
            this.Controls.Add(this.button_natr);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.button_equal);
            this.Controls.Add(this.button_dot);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button_div);
            this.Controls.Add(this.button_puta);
            this.Controls.Add(this.button_minus);
            this.Controls.Add(this.button_plus);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button_ce);
            this.Controls.Add(this.textBox_display);
            this.Name = "Calculator";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_display;
        private System.Windows.Forms.Button button_ce;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_div;
        private System.Windows.Forms.Button button_puta;
        private System.Windows.Forms.Button button_minus;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button_dot;
        private System.Windows.Forms.Button button_equal;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_natr;
        private System.Windows.Forms.Button button_nadr;
        private System.Windows.Forms.Button button_exit;
    }
}

